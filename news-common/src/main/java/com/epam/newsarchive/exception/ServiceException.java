package com.epam.newsarchive.exception;

/**
 * ServiceException occurs due to unexpected behavior
 * of application's service layer and aimed to be a
 * message of operation failure.
 * 
 * @author Heorhi_Bisiaryn
 * @since 2016-02-16
 */

@SuppressWarnings(value = "serial")
public class ServiceException extends Exception {

	public ServiceException() {
	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
