package com.epam.newsarchive.exception;

/**
 * ExceptionDAO occurs due to unexpected behavior
 * of application's DAO layer and aimed to be a
 * message of operation failure and, in some cases,
 * trigger transaction rollback on service layer.
 * 
 * @author Heorhi_Bisiaryn
 * @since 2016-02-16
 */

@SuppressWarnings("serial")
public class ExceptionDAO extends Exception {

	public ExceptionDAO() {
		super();
	}

	public ExceptionDAO(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ExceptionDAO(String message, Throwable cause) {
		super(message, cause);
	}

	public ExceptionDAO(String message) {
		super(message);
	}

	public ExceptionDAO(Throwable cause) {
		super(cause);
	}
	
}
