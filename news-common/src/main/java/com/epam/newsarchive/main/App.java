package com.epam.newsarchive.main;

import java.util.ArrayList;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.FacadeNewsService;
import com.epam.newsarchive.service.impl.FacadeNewsServiceImpl;

public class App {
	
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
		context.getEnvironment().setActiveProfiles("dev");
		context.setConfigLocation("context.xml");
		context.refresh();
//		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
//		context.getEnvironment().setActiveProfiles("dev");
//		context.register(AppConfig.class);
//		context.refresh();
		FacadeNewsService service = (FacadeNewsServiceImpl) context.getBean("FacadeNewsServiceImpl");

		ArrayList<Tag> tags = new ArrayList<>();
		tags.add(new Tag(0, "thug life"));
		tags.add(new Tag(0, "educational"));
		
		try {
			System.out.println(service.fetchByTags(tags, "Великий автор"));
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		context.close();
	}
}
