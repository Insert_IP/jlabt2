package com.epam.newsarchive.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.epam.newsarchive.dao.RoleDAO;
import com.epam.newsarchive.entity.Role;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.RoleService;

@Service("RoleServiceImpl")
@Scope(value = "prototype")
public class RoleServiceImpl implements  RoleService {

	private @Autowired RoleDAO rDao;
	
	@Override
	public void insertRole(Role role) throws ServiceException {
		try{
			rDao.insert(role);
		} catch(ExceptionDAO e){
			throw new ServiceException(e);
		}
	}

}
