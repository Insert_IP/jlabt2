package com.epam.newsarchive.service;

import java.util.ArrayList;

import com.epam.newsarchive.entity.News;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ServiceException;

public interface NewsService {
	
	/**
	 * Transfers arrival's data to DAO layer
	 * @param arrival aggregated object (News + Author + Tag + Comment)
	 * @return arrival id
	 * @throws ServiceException
	 */
	Long addArrival(News news, ArrayList<Long> tagsId, long autId) throws ServiceException;
	
	/**
	 * Transfers an updated version of existing arrival
	 * @param arrival
	 * @return process status
	 * @throws ServiceException
	 */
	boolean edit(News news) throws ServiceException;
	
	/**
	 * Transfers data to remove current arrival
	 * @param arrival id
	 * @return process status
	 * @throws ServiceException
	 */
	boolean delete(long id) throws ServiceException;
	
	/**
	 * Fetches list of news by tags and author
	 * @param list of tags, author's name
	 * @return list of arrival objects
	 * @throws ServiceException
	 */
	ArrayList<News> fetchByTags(ArrayList<Tag> tags, String author) throws ServiceException;
	
	/**
	 * Fetches list of news
	 * @param none
	 * @return list of arrival objects
	 * @throws ServiceException
	 */
	ArrayList<News> fetchArrivals() throws ServiceException;
	
	/**
	 * Counts the list of arrivals
	 * @param none
	 * @return quantity of arrivals
	 * @throws ServiceException
	 */
	int countNews() throws ServiceException;
}
