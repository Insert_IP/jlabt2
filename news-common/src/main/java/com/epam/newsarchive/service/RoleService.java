package com.epam.newsarchive.service;

import com.epam.newsarchive.entity.Role;
import com.epam.newsarchive.exception.ServiceException;

public interface RoleService {
	/**
	 * Inserts role and links with user's
	 * table.
	 * 
	 * @param role
	 * @return role's id
	 * @throws ServiceException
	 */
	void insertRole(Role role) throws ServiceException;
}
