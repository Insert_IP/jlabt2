package com.epam.newsarchive.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsarchive.dao.UserDAO;
import com.epam.newsarchive.entity.User;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.UserService;
import com.epam.newsarchive.transfer.UserTO;

@Service("UserServiceImpl")
@Scope(value = "prototype")
public class UserServiceImpl implements UserService {

	private @Autowired UserDAO uDao;
	
	/** Implementation of {@link UserService#insert(UserTO)}*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Long insert(User user) throws ServiceException {
		Long id = null;
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		try {
			user.setPassword(encoder.encodePassword(user.getPassword(), null));
			id = uDao.insert(user);
		} catch (ExceptionDAO e) {
			throw new ServiceException(e);
		}
		return id;
	}

	/** Implementation of {@link UserService#delete(UserTO)}*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(long id) throws ServiceException {
		try {
			uDao.delete(id);
		} catch (ExceptionDAO e) {
			throw new ServiceException(e);
		}
	}

	/** Implementation of {@link UserService#fetch()}*/
	@Override
	public UserTO fetch() throws ServiceException {
		return null;
	}
	
}
