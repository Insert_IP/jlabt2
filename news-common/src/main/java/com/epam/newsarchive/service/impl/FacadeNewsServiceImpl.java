package com.epam.newsarchive.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsarchive.entity.Author;
import com.epam.newsarchive.entity.Comment;
import com.epam.newsarchive.entity.News;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.AuthorService;
import com.epam.newsarchive.service.CommentService;
import com.epam.newsarchive.service.FacadeNewsService;
import com.epam.newsarchive.service.NewsService;
import com.epam.newsarchive.service.TagService;
import com.epam.newsarchive.transfer.NewsTO;

@Service("FacadeNewsServiceImpl")
@Scope(value = "prototype")
public class FacadeNewsServiceImpl implements FacadeNewsService {

	private @Autowired NewsService nService;

	private @Autowired CommentService cService;

	private @Autowired AuthorService aService;

	private @Autowired TagService tService;

	/** Implementation of {@link FacadeNewsService#addArrival(NewsTO)}*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int addArrival(NewsTO newsTrans) throws ServiceException {
		Long existed;
		Long newTag;
		ArrayList<Long> tagsId = new ArrayList<>();
		for (Tag tag : newsTrans.getTags()) {
			existed = tService.check(tag);
			if (existed == 0) {
				newTag = tService.insert(tag);
				tagsId.add(newTag);
			} else {
				tagsId.add(existed);
			}
		}

		nService.addArrival(newsTrans.getNews(), tagsId, newsTrans.getAuthor().getId());

		return 0;
	}

	/** Implementation of {@link FacadeNewsService#delete(long)}*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean delete(long id) throws ServiceException {
		cService.delete(id);
		nService.delete(id);
		return false;
	}

	/** Implementation of {@link FacadeNewsService#fetchByTags(ArrayList, String)}*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public ArrayList<NewsTO> fetchByTags(ArrayList<Tag> tags, String authSearch) throws ServiceException {
		ArrayList<NewsTO> newsList = new ArrayList<>();
		ArrayList<News> news = nService.fetchByTags(tags, authSearch);

		for (News arrival : news) {
			Author author = aService.getAuthorById(arrival.getId());
			ArrayList<Comment> comments = cService.fetchByNewsId(arrival.getId());
			
			NewsTO nTrans = new NewsTO();
			nTrans.setAuthor(author);
			nTrans.setTags(tags);
			nTrans.setNews(arrival);
			nTrans.setComments(comments);
			
			newsList.add(nTrans);
			
		}
		return newsList;
	}

	/** Implementation of {@link FacadeNewsService#fetchArrivals()}*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public ArrayList<NewsTO> fetchArrivals() throws ServiceException {
		ArrayList<NewsTO> newsList = new ArrayList<>();
		ArrayList<News> news = nService.fetchArrivals();
		
		for (News arrival : news) {
			Author author = aService.getAuthorById(arrival.getId());
			ArrayList<Comment> comments = cService.fetchByNewsId(arrival.getId());
			ArrayList<Tag> tags = tService.fetchTagsById(arrival.getId());
			
			NewsTO nTrans = new NewsTO();
			nTrans.setAuthor(author);
			nTrans.setTags(tags);
			nTrans.setNews(arrival);
			nTrans.setComments(comments);
			
			newsList.add(nTrans);
			
		}
		return newsList;
	}

}
