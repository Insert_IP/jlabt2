package com.epam.newsarchive.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.FacadeUserService;
import com.epam.newsarchive.service.RoleService;
import com.epam.newsarchive.service.UserService;
import com.epam.newsarchive.transfer.UserTO;

@Service("FacadeUserService")
@Scope("prototype")
public class FacadeUserServiceImpl implements FacadeUserService {

	private @Autowired UserService uService;
	private @Autowired RoleService rService;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Long registerUser(UserTO user) throws ServiceException {
		Long id = uService.insert(user.getUser());
		user.getRole().setId(id);
		rService.insertRole(user.getRole());
		return id;
	}
	
}
