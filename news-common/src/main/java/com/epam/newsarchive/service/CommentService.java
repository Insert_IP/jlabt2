package com.epam.newsarchive.service;

import java.util.ArrayList;

import com.epam.newsarchive.entity.Comment;
import com.epam.newsarchive.exception.ServiceException;

public interface CommentService {
	/**
     * Transfers Comment object to DAO layer in order to insert record
     * @param comment, arrival id
     * @throws ServiceException
     */
	void insert(Comment comment, long arrId) throws ServiceException;
	
	/**
     * Transfers comment id to DAO layer in order to delete record
     * @param comment id
     * @throws ServiceException
     */
	void delete(long id) throws ServiceException;
	
	/**
	 * Transfers list of arrival's comments from DAO layer
	 * @param arrival id
	 * @return list of arrival's comments
	 * @throws ServiceException
	 */
	ArrayList<Comment> fetchByNewsId(long id) throws ServiceException;
}
