package com.epam.newsarchive.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.epam.newsarchive.dao.TagDAO;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.TagService;

@Service("TagServiceImpl")
@Scope(value = "prototype")
public class TagServiceImpl implements TagService {

	private @Autowired TagDAO tDao;
	
	/** Implementation of {@link TagService#insert(Tag)}*/
	@Override
	public Long insert(Tag tag) throws ServiceException {
		Long tagId = null;
		
		try {
			tagId = tDao.insert(tag);
		} catch(ExceptionDAO e){
			throw new ServiceException(e);
		}
		
		return tagId;
	}
	
	/** Implementation of {@link TagService#check(Tag)}*/
	@Override
	public Long check(Tag tag) throws ServiceException {
		Long tagId = null;
		
		try{
			tagId = tDao.checkTag(tag);
		} catch(ExceptionDAO e){
			throw new ServiceException(e);
		}
		return tagId;
	}
	
	/** Implementation of {@link TagService#fetchTagsById(int)}*/
	@Override
	public ArrayList<Tag> fetchTagsById(long id) throws ServiceException {
		ArrayList<Tag> tags = null;
		
		try{
			tags = tDao.fetchByNewsId(id);
		} catch(ExceptionDAO e){
			throw new ServiceException(e);
		}
		return tags;
	}

}
