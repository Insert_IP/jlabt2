package com.epam.newsarchive.service;

import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.transfer.UserTO;

public interface FacadeUserService {
	/**
     * Transfers User aggregated object to call Service methods
     * in order to insert multiple records
     * @param user's aggregated object (User + Role)
     * @throws ServiceException
     */
	Long registerUser(UserTO user) throws ServiceException;
}
