package com.epam.newsarchive.service;

import java.util.ArrayList;

import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.transfer.NewsTO;

public interface FacadeNewsService {
	/**
	 * Manipulate's transactional process of arrival insertion
	 * which means multiple INSERT operation
	 * @param arrival aggregated object (News + Author + Tag + Comment)
	 * @return arrival id
	 * @throws ServiceException
	 */
	int addArrival(NewsTO newsTrans) throws ServiceException;
	
	/**
	 * Manipulate's transactional process of arrival removal
	 * @param arrival id
	 * @return process status
	 * @throws ServiceException
	 */
	boolean delete(long id) throws ServiceException;
	
	/**
	 * Fetches list of aggregated objects, including arrivals, tags,
	 * authors and comments by tags and author
	 * @param list of tags, author's name
	 * @return list of arrival objects
	 * @throws ServiceException
	 */
	ArrayList<NewsTO> fetchByTags(ArrayList<Tag> tags, String author) throws ServiceException;
	
	/**
	 * Fetches list of aggregated objects, including arrivals,
	 * tags, authors and comments
	 * @param none
	 * @return list of arrival objects
	 * @throws ServiceException
	 */
	ArrayList<NewsTO> fetchArrivals() throws ServiceException;
}
