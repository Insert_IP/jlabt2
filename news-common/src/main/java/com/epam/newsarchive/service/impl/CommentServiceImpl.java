package com.epam.newsarchive.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.epam.newsarchive.dao.CommentDAO;
import com.epam.newsarchive.entity.Comment;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.CommentService;

@Service("CommentServiceImpl")
@Scope(value = "prototype")
public class CommentServiceImpl implements CommentService {
	
	private @Autowired CommentDAO cDao;
	
	/** Implementation of {@link CommentService#insert(Comment, int)}*/
	@Override
	public void insert(Comment comment, long arrId) throws ServiceException {
		
		try{
			cDao.insert(comment, arrId);
		} catch(ExceptionDAO e){
			throw new ServiceException(e);
		}
	}
	
	/** Implementation of {@link CommentService#delete(int)}*/
	@Override
	public void delete(long id) throws ServiceException {
		
		try{
			cDao.delete(id);
		} catch(ExceptionDAO e){
			throw new ServiceException(e);
		}
	}
	
	/** Implementation of {@link CommentService#fetchByNewsId(int)}*/
	@Override
	public ArrayList<Comment> fetchByNewsId(long id) throws ServiceException {
		ArrayList<Comment> list = null;
		
		try{
			list = cDao.fetch(id);
		} catch(ExceptionDAO e){
			throw new ServiceException(e);
		}
		return list;
	}
	
}
