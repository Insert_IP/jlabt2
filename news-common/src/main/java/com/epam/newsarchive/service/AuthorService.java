package com.epam.newsarchive.service;

import com.epam.newsarchive.entity.Author;
import com.epam.newsarchive.exception.ServiceException;

public interface AuthorService {
	
	/**
     * Transfers Author object to DAO layer in order to insert record
     * @param author
     * @return id of inserted row
     * @throws ServiceException
     */
	int insert(Author author) throws ServiceException;
	
	/**
     * Transfers formed info about author's expiration date
     * @param author
     * @throws ServiceException
     */
	void setExpired(Author author) throws ServiceException;
	
	/**
	 * Gets author by arrival's id
	 * @param arrival id
	 * @return author
	 * @throws ServiceException
	 */
	Author getAuthorById(long id) throws ServiceException;
	
}
