package com.epam.newsarchive.service;

import com.epam.newsarchive.entity.User;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.transfer.UserTO;

public interface UserService {
	
	/**
     * Transfers User aggregated object to call DAO methods
     * in order to insert multiple records
     * @param user's aggregated object (User + Role)
     * @throws ServiceException
     */
	Long insert(User user) throws ServiceException;
	
	/**
     * Process delete records from multiple tables
     * @param user's aggregated object (User + Role)
     * @throws ServiceException
     */
	void delete(long id) throws ServiceException;
	
	/**
     * Forms response to client which contains aggregated object.
     * It include user and role info
     * @param none
     * @return aggregated object (User + Role)
     * @throws ServiceException
     */
	UserTO fetch() throws ServiceException;
}
