package com.epam.newsarchive.service;

import java.util.ArrayList;

import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ServiceException;

public interface TagService {
	
	/**
     * Transfers Tag object to DAO layer in order to insert record
     * @param tag
     * @throws ServiceException
     */
	Long insert(Tag tag) throws ServiceException;
	
	/**
	 * Checks the presence of current tag
	 * @param tag
	 * @return id of inserted tag
	 * @throws ServiceException
	 */
	Long check(Tag tag) throws ServiceException;
	
	/**
	 * Transfers list of arrival's tags from DAO layer
	 * @param id
	 * @return list of arrival's tags
	 * @throws ServiceException
	 */
	ArrayList<Tag> fetchTagsById(long id) throws ServiceException;
}
