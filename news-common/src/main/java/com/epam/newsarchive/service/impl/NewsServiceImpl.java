package com.epam.newsarchive.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsarchive.dao.NewsDAO;
import com.epam.newsarchive.entity.News;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.NewsService;

@Service("NewsServiceImpl")
@Scope(value = "prototype")
public class NewsServiceImpl implements NewsService {

	private @Autowired NewsDAO nDao;

	/** Implementation of {@link NewsService#addArrival(News, ArrayList, Integer)}*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Long addArrival(News news, ArrayList<Long> tagsId, long autId) throws ServiceException {
		Long newsId = null;

		try {
			newsId = nDao.insert(news);

			nDao.linkToAuthor(newsId, autId);
			nDao.linkToTag(newsId, tagsId);
		} catch (ExceptionDAO e) {
			throw new ServiceException(e);
		}
		return newsId;
	}

	/** Implementation of {@link NewsService#fetchArrivals()}*/
	@Override
	public ArrayList<News> fetchArrivals() throws ServiceException {
		ArrayList<News> arrivals = null;
		
		try {
			arrivals = nDao.fetch();
		} catch (ExceptionDAO e) {
			throw new ServiceException(e);
		}
		return arrivals;
	}

	/** Implementation of {@link NewsService#delete(int)}*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean delete(long id) throws ServiceException {
		try {
			nDao.unlinkFromAuthor(id);
			nDao.unlinkFromTag(id);
			nDao.delete(id);
		} catch (ExceptionDAO e) {
			throw new ServiceException(e);
		}

		return true;
	}

	/** Implementation of {@link NewsService#fetchByTags(ArrayList, String)}*/
	@Override
	public ArrayList<News> fetchByTags(ArrayList<Tag> tags, String authSearch) throws ServiceException {
		ArrayList<News> arrivals = null;

		try {
			arrivals = nDao.fetchByTags(tags, authSearch);
		} catch (ExceptionDAO e) {
			throw new ServiceException(e);
		}
		return arrivals;
	}

	/** Implementation of {@link NewsService#edit(News)}*/
	@Override
	public boolean edit(News news) throws ServiceException {
		boolean status = false;

		try {
			status = nDao.edit(news);
		} catch (ExceptionDAO e) {
			throw new ServiceException(e);
		}

		return status;
	}

	/** Implementation of {@link NewsService#countNews()}*/
	@Override
	public int countNews() throws ServiceException {
		int count;

		try {
			count = nDao.countNews();
		} catch (ExceptionDAO e) {
			throw new ServiceException(e);
		}

		return count;
	}

}
