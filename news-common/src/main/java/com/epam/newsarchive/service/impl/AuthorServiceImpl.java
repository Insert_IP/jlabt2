package com.epam.newsarchive.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.epam.newsarchive.dao.AuthorDAO;
import com.epam.newsarchive.entity.Author;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.AuthorService;

@Service("AuthorServiceImpl")
@Scope(value = "prototype")
public class AuthorServiceImpl implements AuthorService {
	
	private @Autowired AuthorDAO aDao;
	
	/** Implementation of {@link AuthorService#insert(Author)}*/
	@Override
	public int insert(Author author) throws ServiceException {
		int id = 0;
		
		try{
			id = aDao.insert(author);
		} catch(ExceptionDAO e){
			throw new ServiceException(e);
		}
		
		return id;
	}
	
	/** Implementation of {@link AuthorService#setExpired(Author)}*/
	@Override
	public void setExpired(Author author) throws ServiceException {
		try{
			aDao.setExpired(author);
		} catch(ExceptionDAO e){
			throw new ServiceException(e);
		}
	}
	
	/** Implementation of {@link AuthorService#getAuthorById(int)}*/
	@Override
	public Author getAuthorById(long id) throws ServiceException {
		Author author = null;
		
		try{
			author = aDao.fetchByNewsId(id);
		} catch(ExceptionDAO e){
			throw new ServiceException(e);
		}
		return author;
	}
	
}
