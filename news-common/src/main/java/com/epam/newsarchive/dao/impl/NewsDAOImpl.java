package com.epam.newsarchive.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsarchive.dao.NewsDAO;
import com.epam.newsarchive.dao.query.QueryGenerator;
import com.epam.newsarchive.entity.News;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ExceptionDAO;

@Component
public class NewsDAOImpl implements NewsDAO {

	private final String INSERT_ARRIVAL = "INSERT INTO NEWS(NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES(NWS_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";
	private final String INSERT_LINK_TAG = "INSERT INTO NEWS_TAGS(NT_NEWS_ID, NT_TAG_ID) VALUES(?, ?)";
	private final String INSERT_LINK_AUTHOR = "INSERT INTO NEWS_AUTHORS(NA_NEWS_ID, NA_AUTHOR_ID) VALUES(?, ?)";
	private final String DELETE_ARRIVAL = "DELETE FROM NEWS WHERE NWS_NEWS_ID = ?";
	private final String FETCH_ALL = "SELECT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE FROM NEWS ORDER BY NWS_CREATION_DATE";
	private final String UPDATE_ARRIVAL = "UPDATE NEWS SET NWS_TITLE = ?, NWS_SHORT_TEXT = ?, NWS_FULL_TEXT = ?, NWS_MODIFICATION_DATE = ? WHERE NWS_NEWS_ID = ?";
	private final String FETCH_BY_ID = "SELECT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE FROM NEWS WHERE NWS_NEWS_ID = ?";
	private final String COUNT_NEWS = "SELECT COUNT(*) FROM NEWS";
	private final String DELETE_LINK_TAG = "DELETE FROM NEWS_TAGS WHERE NT_NEWS_ID = ?";
	private final String DELETE_LINK_AUTHOR = "DELETE FROM NEWS_AUTHORS WHERE NA_NEWS_ID = ?";
	
	private @Autowired DataSource dataSource;
	
	/** Implementation of {@link NewsDAO#insert(News)}*/
	@Override
	public Long insert(News news) throws ExceptionDAO {
		Long id = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(INSERT_ARRIVAL, new String[] { "NWS_NEWS_ID" })) {
			
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getSht_text());
			ps.setString(3, news.getText());
			ps.setTimestamp(4, Timestamp.valueOf(news.getCreation()));
			ps.setTimestamp(5, Timestamp.valueOf(news.getModification()));
			ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();

			while (rs.next()) {
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return id;
	}

	/** Implementation of {@link NewsDAO#linkToTag(int, ArrayList)}*/
	@Override
	public boolean linkToTag(long newsId, ArrayList<Long> tags) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(INSERT_LINK_TAG)) {

			for (long tag : tags) {
				ps.setLong(1, newsId);
				ps.setLong(2, tag);
				ps.addBatch();
			}

			ps.executeBatch();
			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}

	/** Implementation of {@link NewsDAO#linkToAuthor(int, int)}*/
	@Override
	public boolean linkToAuthor(long newsId, long authorId) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(INSERT_LINK_AUTHOR)) {
			
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();

			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}

	/** Implementation of {@link NewsDAO#fetch()}*/
	@Override
	public ArrayList<News> fetch() throws ExceptionDAO {
		ArrayList<News> list = null;
		News news;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(FETCH_ALL)) {
			
			ResultSet rs = ps.executeQuery();

			list = new ArrayList<>();

			while (rs.next()) {
				news = new News();
				news.setId(rs.getInt(1));
				news.setTitle(rs.getString(2));
				news.setSht_text(rs.getString(3));
				news.setText(rs.getString(4));
				news.setCreation(rs.getTimestamp(5).toLocalDateTime());
				news.setModification(rs.getTimestamp(6).toLocalDateTime());
				list.add(news);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return list;
	}

	/** Implementation of {@link NewsDAO#delete(int)}*/
	@Override
	public boolean delete(long id) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(DELETE_ARRIVAL)) {
			
			ps.setLong(1, id);
			ps.executeUpdate();
			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}

	/** Implementation of {@link NewsDAO#edit(News)}*/
	@Override
	public boolean edit(News news) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(UPDATE_ARRIVAL)) {
			
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getSht_text());
			ps.setString(3, news.getText());
			ps.setTimestamp(4, Timestamp.valueOf(news.getModification()));
			ps.setLong(5, news.getId());
			ps.executeUpdate();

			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}

	/** Implementation of {@link NewsDAO#fetchById(int)}*/
	@Override
	public News fetchById(long id) throws ExceptionDAO {
		News news = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(FETCH_BY_ID)) {
			
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				news = new News();
				news.setId(rs.getInt(1));
				news.setTitle(rs.getString(2));
				news.setSht_text(rs.getString(3));
				news.setText(rs.getString(4));
				news.setCreation(rs.getTimestamp(5).toLocalDateTime());
				news.setModification(rs.getTimestamp(6).toLocalDateTime());
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return news;
	}
	
	/** Implementation of {@link NewsDAO#fetchByTags(ArrayList, String)}*/
	@Override
	public ArrayList<News> fetchByTags(ArrayList<Tag> tags, String author) throws ExceptionDAO {
		int i = 0;
		ArrayList<News> list = null;
		News news;
		Connection con = DataSourceUtils.getConnection(dataSource);
		String query = QueryGenerator.querySearchByTags(tags.size());
		try(PreparedStatement ps = con.prepareStatement(query)) {
			
			for(Tag tag : tags){
				i++;
				ps.setString(i, tag.getTag());
			}
			ps.setString(++i, author);
			ResultSet rs = ps.executeQuery();
			
			list = new ArrayList<>();
			while (rs.next()) {
				news = new News();
				news.setId(rs.getInt(1));
				news.setTitle(rs.getString(2));
				news.setSht_text(rs.getString(3));
				news.setText(rs.getString(4));
				news.setCreation(rs.getTimestamp(5).toLocalDateTime());
				list.add(news);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return list;
	}
	
	/** Implementation of {@link NewsDAO#countNews()}*/
	@Override
	public int countNews() throws ExceptionDAO {
		int count = 0;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(COUNT_NEWS)) {
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return count;
	}
	
	/** Implementation of {@link NewsDAO#unlinkFromAuthor(int)}*/
	@Override
	public boolean unlinkFromAuthor(long id) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(DELETE_LINK_AUTHOR)) {
			ps.setLong(1, id);
			ps.executeUpdate();
			
			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}
	
	/** Implementation of {@link NewsDAO#unlinkFromTag(int)}*/
	@Override
	public boolean unlinkFromTag(long id) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(DELETE_LINK_TAG)) {
			ps.setLong(1, id);
			ps.executeUpdate();
			
			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}

}
