package com.epam.newsarchive.dao;

import java.util.ArrayList;

import com.epam.newsarchive.entity.User;
import com.epam.newsarchive.exception.ExceptionDAO;

public interface UserDAO {
	
	/**
     * Inserts user into USERS table
     * @param user
     * @return id of inserted row
     * @throws ExceptionDAO
     */
	Long insert(User user) throws ExceptionDAO;
	
	/**
     * Removes record from USERS table
     * @param user id
     * @return process status
     * @throws ExceptionDAO
     */
	boolean delete(long id) throws ExceptionDAO;
	
	/**
     * Fetches the whole list of users
     * @param none
     * @return list of users
     * @throws ExceptionDAO
     */
	ArrayList<User> fetch() throws ExceptionDAO;
}
