package com.epam.newsarchive.dao;

import com.epam.newsarchive.entity.Author;
import com.epam.newsarchive.exception.ExceptionDAO;

public interface AuthorDAO {
	
	/**
     * Inserts record into AUTHORS table
     * @param author
     * @return id of inserted row
     * @throws ExceptionDAO
     */
	int insert(Author author) throws ExceptionDAO;
	
	/**
     * Fetches author by id of arrival
     * @param arrival id
     * @return author
     * @throws ExceptionDAO
     */
	Author fetchByNewsId(long id) throws ExceptionDAO;
	
	/**
     * Sets author's expiration date
     * @param author
     * @return process status
     * @throws ExceptionDAO
     */
	boolean setExpired(Author author) throws ExceptionDAO;
	
	/**
     * Checks the presence of record in AUTHORS table
     * @param author
     * @return zero if author don't exists, or author's id
     * @throws ExceptionDAO
     */
	int check(Author author) throws ExceptionDAO;
}
