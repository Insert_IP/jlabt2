package com.epam.newsarchive.dao.query;

/**
 * The QueryGenerator is a utility tool aimed to construct
 * queries to kind of complicated purposes, such as Search
 * Criteria and ed esta. There is a bits of queries which
 * methods include for current SQL operation.
 * 
 * @author Heorhi_Bisiaryn
 * @since 2016-02-27
 */
public abstract class QueryGenerator {

	private static final String FETCH_BY_TAGS = "SELECT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE FROM NEWS "
			+ "INNER JOIN NEWS_TAGS ON NEWS.NWS_NEWS_ID = NEWS_TAGS.NT_NEWS_ID "
			+ "INNER JOIN TAGS ON NEWS_TAGS.NT_TAG_ID = TAGS.TAG_ID "
			+ "INNER JOIN NEWS_AUTHORS ON NEWS.NWS_NEWS_ID = NEWS_AUTHORS.NA_NEWS_ID "
			+ "INNER JOIN AUTHORS ON NEWS_AUTHORS.NA_AUTHOR_ID = AUTHORS.AUT_AUTHOR_ID WHERE TAGS.TAG_NAME IN(?";
	private static final String AUTHOR_FILTER = ") AND AUT_AUTHOR_NAME = ?";

	/**
	 * Constructs query to fetch arrival using
	 * it's aggregate of tags and author's name.
	 * 
	 * @param quantity of tags
	 * @return constructed SQL-query
	 */
	public static String querySearchByTags(int quantity) {
		String tagMark = ", ?";
		StringBuffer result = new StringBuffer(FETCH_BY_TAGS);

		for (int i = 0; i < quantity - 1; i++) {
			result.append(tagMark);
		}
		result.append(AUTHOR_FILTER);

		return result.toString();
	}

}
