package com.epam.newsarchive.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsarchive.dao.TagDAO;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ExceptionDAO;

@Component
public class TagDAOImpl implements TagDAO {
	
	private final String INSERT_TAG = "INSERT INTO TAGS(TAG_ID, TAG_NAME) VALUES(TAG_SEQ.NEXTVAL, ?)";
	private final String FETCH_BY_NEWS_ID = "SELECT TAG_NAME FROM TAGS INNER JOIN NEWS_TAGS ON NEWS_TAGS.NT_TAG_ID = TAGS.TAG_ID WHERE NEWS_TAGS.NT_NEWS_ID = ?";
	private final String SELECT_TAG = "SELECT TAG_ID FROM TAGS WHERE TAG_NAME = ?";
	
	private @Autowired DataSource dataSource;

	/** Implementation of {@link TagDAO#insertTags(ArrayList)}*/
	@Override
	public ArrayList<Integer> insertTags(ArrayList<Tag> tags) throws ExceptionDAO {
		ArrayList<Integer> id = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(INSERT_TAG, new String[]{"TAG_ID"})) {
			
			for(Tag tag : tags){
				ps.setString(1, tag.getTag());
				
				ps.addBatch();
				ps.executeBatch();
			}
			
			ResultSet rs = ps.getGeneratedKeys();
			id = new ArrayList<>();
			
			while(rs.next()){
				id.add(rs.getInt(1));
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally{
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return id;
	}
	
	/** Implementation of {@link TagDAO#fetchByNewsId(int)}*/
	@Override
	public ArrayList<Tag> fetchByNewsId(long id) throws ExceptionDAO {
		ArrayList<Tag> tags = null;
		Tag tag;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(FETCH_BY_NEWS_ID)) {
			
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			tags = new ArrayList<>();
			while(rs.next()){
				tag = new Tag();
				tag.setTag(rs.getString(1));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tags;
	}
	
	/** Implementation of {@link TagDAO#insert(Tag)}*/
	@Override
	public Long insert(Tag tag) throws ExceptionDAO {
		Long id = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(INSERT_TAG, new String[]{"TAG_ID"})) {
			
			ps.setString(1, tag.getTag());
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			
			while(rs.next()){
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return id;
	}
	
	/** Implementation of {@link TagDAO#checkTag(Tag)}*/
	@Override
	public Long checkTag(Tag tag) throws ExceptionDAO {
		Long id = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(SELECT_TAG)) {
			ps.setString(1, tag.getTag());
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return id;
	}
	
}
