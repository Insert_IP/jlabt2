package com.epam.newsarchive.dao;

import com.epam.newsarchive.entity.Role;
import com.epam.newsarchive.exception.ExceptionDAO;

public interface RoleDAO {
	
	/**
     * Inserts role into ROLES table
     * @param role
     * @return process status
     * @throws ExceptionDAO 
     */
	public boolean insert(Role role) throws ExceptionDAO;
	
	/**
     * Removes role from ROLES table
     * @param role
     * @return process status
     * @throws ExceptionDAO 
     */
	public boolean delete(Role role) throws ExceptionDAO;
	
	/**
     * Fetches role from ROLES table
     * @param user id
     * @return role name
     * @throws ExceptionDAO
     */
	public Role fetch(long id) throws ExceptionDAO;
}
