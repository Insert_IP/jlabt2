package com.epam.newsarchive.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsarchive.dao.AuthorDAO;
import com.epam.newsarchive.entity.Author;
import com.epam.newsarchive.exception.ExceptionDAO;

@Component
public class AuthorDAOImpl implements AuthorDAO {

	private final String INSERT_AUTHOR = "INSERT INTO AUTHORS(AUT_AUTHOR_ID, AUT_AUTHOR_NAME) VALUES(AUT_SEQ.NEXTVAL, ?)";
	private final String FETCH_BY_NEWS_ID = "SELECT AUT_AUTHOR_NAME FROM AUTHORS INNER JOIN NEWS_AUTHORS ON AUTHORS.AUT_AUTHOR_ID = NEWS_AUTHORS.NA_AUTHOR_ID WHERE NEWS_AUTHORS.NA_NEWS_ID = ?";
	private final String SET_EXPIRED = "UPDATE AUTHORS SET AUT_EXPIRED = ? WHERE AUT_AUTHOR_ID = ?";
	private final String CHECK_AUTHOR = "SELECT AUT_AUTHOR_ID FROM AUTHORS WHERE AUT_AUTHOR_NAME = ?";
	
	private @Autowired DataSource dataSource;

	/** Implementation of {@link AuthorDAO#insert(Author)} */
	@Override
	public int insert(Author author) throws ExceptionDAO {
		int id = 0;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(INSERT_AUTHOR, new String[] { "AUT_AUTHOR_ID" })) {

			ps.setString(1, author.getName());
			ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();
			while (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return id;
	}

	/** Implementation of {@link AuthorDAO#fetchByNewsId(int)} */
	@Override
	public Author fetchByNewsId(long id) throws ExceptionDAO {
		Author author = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(FETCH_BY_NEWS_ID)) {
			
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				author = new Author();
				author.setName(rs.getString(1));
			}

		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return author;
	}
	
	/** Implementation of {@link AuthorDAO#setExpired(Author)} */
	@Override
	public boolean setExpired(Author author) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(SET_EXPIRED)) {
			
			ps.setTimestamp(1, Timestamp.valueOf(author.getExpired()));
			ps.setLong(2, author.getId());
			ps.executeUpdate();
			
			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}
	
	/** Implementation of {@link AuthorDAO#check(Author)} */
	public int check(Author author) throws ExceptionDAO{
		int id = 0;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(CHECK_AUTHOR)) {
			ps.setString(1, author.getName());
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return id;
	}

}
