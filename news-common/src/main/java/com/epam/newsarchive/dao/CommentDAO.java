package com.epam.newsarchive.dao;

import java.util.ArrayList;

import com.epam.newsarchive.entity.Comment;
import com.epam.newsarchive.exception.ExceptionDAO;

public interface CommentDAO {
	
	/**
     * Inserts comment, related to current arrival
     * @param comment, arrival id
     * @return process status
     * @throws ExceptionDAO
     */
	boolean insert(Comment comment, long arrID) throws ExceptionDAO;
	
	/**
     * Removes comment from COMMENTS table
     * @param comment id
     * @return process status
     * @throws ExceptionDAO
     */
	boolean delete(long id) throws ExceptionDAO;
	
	/**
     * Fetches comments from COMMENTS table
     * @param arrival id
     * @return list of comments
     * @throws ExceptionDAO
     */
	ArrayList<Comment> fetch(long id) throws ExceptionDAO;
}
