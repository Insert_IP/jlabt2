package com.epam.newsarchive.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsarchive.dao.CommentDAO;
import com.epam.newsarchive.entity.Comment;
import com.epam.newsarchive.exception.ExceptionDAO;

@Component
public class CommentDAOImpl implements CommentDAO {
	
	private final String INSERT_COMMENT = "INSERT INTO COMMENTS(CMT_COMMENT_ID, CMT_NEWS_ID, CMT_COMMENT_TEXT, CMT_CREATION_DATE) VALUES(CMT_SEQ.NEXTVAL, ?, ?, ?)";
	private final String DELETE_COMMENT = "DELETE FROM COMMENTS WHERE CMT_NEWS_ID = ?";
	private final String FETCH_COMMENTS = "SELECT CMT_COMMENT_ID, CMT_COMMENT_TEXT, CMT_CREATION_DATE FROM COMMENTS WHERE CMT_NEWS_ID = ?";
	
	private @Autowired DataSource dataSource;

	/** Implementation of {@link CommentDAO#insert(Comment, int)} */
	@Override
	public boolean insert(Comment comment, long arrID) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(INSERT_COMMENT)) {
			
			ps.setLong(1, arrID);
			ps.setString(2, comment.getComment());
			ps.setTimestamp(3, Timestamp.valueOf(comment.getCreated()));
			ps.executeUpdate();

			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}
	
	/** Implementation of {@link CommentDAO#delete(int)} */
	@Override
	public boolean delete(long id) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(DELETE_COMMENT)) {
			
			ps.setLong(1, id);
			ps.executeUpdate();
			
			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}
	
	/** Implementation of {@link CommentDAO#fetch(int)} */
	@Override
	public ArrayList<Comment> fetch(long id) throws ExceptionDAO {
		ArrayList<Comment> comments = null;
		Comment comment;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(FETCH_COMMENTS)) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			
			comments = new ArrayList<>();
			while(rs.next()){
				comment = new Comment();
				comment.setId(rs.getInt(1));
				comment.setComment(rs.getString(2));
				comment.setCreated(rs.getTimestamp(3).toLocalDateTime());
				
				comments.add(comment);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return comments;
	}
}
