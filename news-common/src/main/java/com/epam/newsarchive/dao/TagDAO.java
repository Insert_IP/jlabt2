package com.epam.newsarchive.dao;

import java.util.ArrayList;

import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ExceptionDAO;

public interface TagDAO {
	
	/**
     * Inserts tag record into TAGS table
     * @param tag
     * @return tag id
     * @throws ExceptionDAO
     */
	Long insert(Tag tag) throws ExceptionDAO;
	
	/**
     * Checks the presence of current tag
     * @param tag
     * @return zero, if there is no appropriate tag, or tag's id which was found
     * @throws ExceptionDAO
     */
	Long checkTag(Tag tag) throws ExceptionDAO;
	
	/**
     * Inserts multiple rows into TAGS table
     * @param list of tags
     * @return list of row's ids, which was inserted
     * @throws ExceptionDAO
     */
	ArrayList<Integer> insertTags(ArrayList<Tag> obj) throws ExceptionDAO;
	
	/**
     * Fetches tags which have the reference to current arrival
     * @param arrival id
     * @return list of tags
     * @throws ExceptionDAO
     */
	ArrayList<Tag> fetchByNewsId(long id) throws ExceptionDAO;
}
