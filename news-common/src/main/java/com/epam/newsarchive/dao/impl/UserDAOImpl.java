package com.epam.newsarchive.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsarchive.dao.UserDAO;
import com.epam.newsarchive.entity.User;
import com.epam.newsarchive.exception.ExceptionDAO;

@Component
public class UserDAOImpl implements UserDAO {
	
	private final String INSERT_USER = "INSERT INTO USERS(USR_USER_ID, USR_USER_NAME, USR_LOGIN, USR_PASSWORD) VALUES(USR_SEQ.NEXTVAL, ?, ?, ?)";
	private final String FETCH_ALL = "SELECT USR_USER_ID, USR_USER_NAME FROM USERS";
	private final String DELETE_USER = "DELETE FROM USERS WHERE USR_USER_ID = ?";
	
	private @Autowired DataSource dataSource;
	
	@Override
	public Long insert(User user) throws ExceptionDAO {
		Long id = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(INSERT_USER, new String[]{"USR_USER_ID"})) {
			
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getLogin());
			ps.setString(3, user.getPassword());
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			
			while(rs.next()){
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return id;
	}
	
	@Override
	public ArrayList<User> fetch() throws ExceptionDAO {
		User user;
		ArrayList<User> list = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(FETCH_ALL)) {
			
			ResultSet rs = ps.executeQuery();
			
			list = new ArrayList<>();
			
			while(rs.next()){
				user = new User();
				user.setId(rs.getInt(1));
				user.setUsername(rs.getString(2));
				user.setLogin(rs.getString(3));
				user.setPassword(rs.getString(4));
				
				list.add(user);
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return list;
	}
	
	@Override
	public boolean delete(long id) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(DELETE_USER)) {
			
			ps.setLong(1, id);
			ps.executeUpdate();
			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}
}
