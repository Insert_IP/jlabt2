package com.epam.newsarchive.dao;

import java.util.ArrayList;

import com.epam.newsarchive.entity.News;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ExceptionDAO;

public interface NewsDAO {
	
	/**
     * Inserts arrival record into NEWS table
     * @param arrival
     * @return id of inserted row 
     * @throws ExceptionDAO
     */
	Long insert(News obj) throws ExceptionDAO;
	
	/**
     * Removes arrival record from NEWS table
     * @param arrival id
     * @return process status 
     * @throws ExceptionDAO
     */
	boolean delete(long id) throws ExceptionDAO;
	
	/**
     * Updates arrival record in NEWS table
     * @param News object with updated data
     * @return process status 
     * @throws ExceptionDAO
     */
	boolean edit(News news) throws ExceptionDAO;
	
	/**
     * Inserts multiple records into linking NEWS_TAGS table
     * @param arrival id, list of tag's id
     * @return process status
     * @throws ExceptionDAO
     */
	boolean linkToTag(long newsId, ArrayList<Long> tags) throws ExceptionDAO;
	
	/**
     * Inserts record into linking NEWS_AUTHORS table
     * @param arrival id, author id
     * @return process status 
     * @throws ExceptionDAO
     */
	boolean linkToAuthor(long newsId, long authorId) throws ExceptionDAO;
	
	/**
     * Removes from linking table NEWS_TAGS multiple records
     * @param arrival id, author id
     * @return process status 
     * @throws ExceptionDAO
     */
	boolean unlinkFromTag(long id) throws ExceptionDAO;
	
	/**
     * Removes from linking table NEWS_AUTHORS multiple records
     * @param arrival id
     * @return process status 
     * @throws ExceptionDAO
     */
	boolean unlinkFromAuthor(long id) throws ExceptionDAO;
	
	/**
     * Fetches the whole list of news from NEWS table
     * @param none
     * @return list of news
     * @throws ExceptionDAO
     */
	ArrayList<News> fetch() throws ExceptionDAO;
	
	/**
     * Fetches arrival from NEWS table by id parameter
     * @param arrival id
     * @return arrival
     * @throws ExceptionDAO
     */
	News fetchById(long id) throws ExceptionDAO;
	
	/**
     * Fetches list of news which are generalized by list of tags 
     * @param list of tags
     * @return list of news
     * @throws ExceptionDAO
     */
	ArrayList<News> fetchByTags(ArrayList<Tag> tags, String author) throws ExceptionDAO;
	
	/**
     * Counts the quantity of arrivals
     * @param none
     * @return quantity of arrivals
     * @throws ExceptionDAO
     */
	int countNews() throws ExceptionDAO;
}
