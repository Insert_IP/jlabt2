package com.epam.newsarchive.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsarchive.dao.RoleDAO;
import com.epam.newsarchive.entity.Role;
import com.epam.newsarchive.exception.ExceptionDAO;

@Component
public class RoleDAOImpl implements RoleDAO {
	
	private final String INSERT_ROLE = "INSERT INTO ROLES(RLS_USER_ID, RLS_ROLE_NAME) VALUES(?, ?)";
	private final String DELETE_ROLE = "DELETE FROM ROLES WHERE RLS_USER_ID = ? AND RLS_ROLE_NAME = ?";
	private final String FETCH_ROLE = "SELECT RLS_ROLE_NAME WHERE RLS_USER_ID = ?";
	
	private @Autowired DataSource dataSource;

	@Override
	public boolean insert(Role role) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(INSERT_ROLE)) {
			ps.setLong(1, role.getId());
			ps.setString(2, role.getRoleName());
			ps.executeUpdate();
			
			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return status;
	}

	@Override
	public boolean delete(Role role) throws ExceptionDAO {
		boolean status = false;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(DELETE_ROLE)) {
			ps.setLong(1, role.getId());
			ps.setString(2, role.getRoleName());
			ps.executeUpdate();
			
			status = true;
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		
		return status;
	}

	@Override
	public Role fetch(long id) throws ExceptionDAO {
		Role role = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try(PreparedStatement ps = con.prepareStatement(FETCH_ROLE)) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			
			role = new Role();
			while(rs.next()){
				role.setRoleName(rs.getString(1));
			}
		} catch (SQLException e) {
			throw new ExceptionDAO(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return role;
	}

}
