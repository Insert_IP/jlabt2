package com.epam.newsarchive.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsarchive.dao.TagDAO;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.epam.newsarchive.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/conf/test-context.xml")
@ActiveProfiles("test")
public class TagServiceTest {

	@Mock
	private TagDAO tDao;
	
	@InjectMocks
	private TagServiceImpl tService;
	
	private Tag tag;
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		
		tag = new Tag(1, "test");
	}
	
	@Test
	public void insertTest() {
		try{
			tService.insert(tag);
			
			verify(tDao, times(1)).insert(tag);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void checkTest() {
		try{
			tService.check(tag);
			
			verify(tDao, times(1)).checkTag(tag);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void fetchTagsByIdTest() {
		try{
			tService.fetchTagsById(1);
			
			verify(tDao, times(1)).fetchByNewsId(1);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}

}
