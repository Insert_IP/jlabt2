package com.epam.newsarchive.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsarchive.entity.Author;
import com.epam.newsarchive.entity.News;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.AuthorService;
import com.epam.newsarchive.service.CommentService;
import com.epam.newsarchive.service.NewsService;
import com.epam.newsarchive.service.TagService;
import com.epam.newsarchive.transfer.NewsTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/conf/test-context.xml")
@ActiveProfiles("test")
public class FacadeNewsServiceTest {
	
	private @Mock NewsService nService;
	private @Mock AuthorService aService;
	private @Mock TagService tService;
	private @Mock CommentService cService;

	@InjectMocks
	private FacadeNewsServiceImpl nFacadeService;
	
	private NewsTO nTrans;
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		
		nTrans = new NewsTO();
		nTrans.setNews(new News());
		nTrans.getNews().setId(1L);
		nTrans.setAuthor(new Author());
		nTrans.getAuthor().setId(1L);
		nTrans.setTags(new ArrayList<Tag>());
	}
	
	@Test
	public void insertTest(){
		try{
			nFacadeService.addArrival(nTrans);
			
			verify(nService, times(1)).addArrival(nTrans.getNews(), new ArrayList<>(), 1L);
		} catch(ServiceException e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void deleteTest() {
		try{
			nFacadeService.delete(1);
			
			verify(cService, times(1)).delete(1);
			verify(nService, times(1)).delete(1);
		} catch(ServiceException e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void fetchByTagsTest() {
		try{
			nFacadeService.fetchByTags(new ArrayList<Tag>(), "author");
			
			verify(nService, times(1)).fetchByTags(nTrans.getTags(), "author");
		} catch(ServiceException e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void fetchArrivalsTest() {
		try{
			nFacadeService.fetchArrivals();
			
			verify(nService, times(1)).fetchArrivals();
		} catch(ServiceException e){
			fail("Service execution failed");
		}
	}

}
