package com.epam.newsarchive.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsarchive.dao.AuthorDAO;
import com.epam.newsarchive.entity.Author;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.epam.newsarchive.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/conf/test-context.xml")
@ActiveProfiles("test")
public class AuthorServiceTest {
	
	private @Mock AuthorDAO aDao;
	
	@InjectMocks
	private AuthorServiceImpl aService;
	
	private Author author;
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		
		author = new Author(1, "test", null);
	}
	
	@Test
	public void setExpiredTest() throws ServiceException{
		try{
			aService.setExpired(author);
			
			verify(aDao, times(1)).setExpired(author);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void insertTest() {
		try{
			aService.insert(author);
			
			verify(aDao, times(1)).insert(author);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void getAuthorByIdTest() {
		try{
			aService.getAuthorById(1);
			
			verify(aDao, times(1)).fetchByNewsId(1);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
}
