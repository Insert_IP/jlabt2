package com.epam.newsarchive.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsarchive.dao.NewsDAO;
import com.epam.newsarchive.entity.News;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.epam.newsarchive.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/conf/test-context.xml")
@ActiveProfiles("test")
public class NewsServiceTest {

	private @Mock NewsDAO nDao;
	
	@InjectMocks
	private NewsServiceImpl nService;
	
	private News news;
	private ArrayList<Long> listTags;
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		
		news = new News(1L, "test", "test", "test", LocalDateTime.now(), LocalDateTime.now());
		
		listTags = new ArrayList<>();
		listTags.add(4L);
	}
	
	@Test
	public void addArrivalTest() {
		try{
			nService.addArrival(news, listTags, 3);
			
			verify(nDao, times(1)).insert(news);
			verify(nDao, times(1)).linkToAuthor(anyLong(), anyLong());
			verify(nDao, times(1)).linkToTag(anyLong(), eq(listTags));
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void editTest() {
		try{
			nService.edit(news);
			
			verify(nDao, times(1)).edit(news);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void deleteTest() {
		try{
			nService.delete(1);
			
			verify(nDao, times(1)).delete(1);
			verify(nDao, times(1)).unlinkFromAuthor(1);
			verify(nDao, times(1)).unlinkFromTag(1);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void fetchByTagsTest() {
		try{
			nService.fetchByTags(new ArrayList<>(), "author");
			
			verify(nDao, times(1)).fetchByTags(any(), eq("author"));
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void fetchArrivalsTest() {
		try{
			nService.fetchArrivals();
			
			verify(nDao, times(1)).fetch();
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void countNewsTest() {
		try{
			nService.countNews();
			
			verify(nDao, times(1)).countNews();
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}

}
