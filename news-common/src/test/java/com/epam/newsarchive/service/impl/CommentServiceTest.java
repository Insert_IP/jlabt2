package com.epam.newsarchive.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsarchive.dao.CommentDAO;
import com.epam.newsarchive.entity.Comment;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.NewsService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/conf/test-context.xml")
@ActiveProfiles("test")
public class CommentServiceTest {

	private @Mock CommentDAO cDao;
	private @Mock NewsService nService;
	
	@InjectMocks
	private CommentServiceImpl cService;
	
	private Comment comment;
	
	@Before
	public void init(){
		comment = new Comment(1, "test", LocalDateTime.now());
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void insertTest() {
		try{
			cService.insert(comment, 1);
			verify(cDao, times(1)).insert(comment, 1);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void deleteTest() {
		try{
			cService.delete(1);
			verify(cDao, times(1)).delete(1);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}
	
	@Test
	public void fetchByNewsIdTest() {
		try{
			cService.fetchByNewsId(1);
			verify(cDao, times(1)).fetch(1);
		} catch(ServiceException | ExceptionDAO e){
			fail("Service execution failed");
		}
	}

}
