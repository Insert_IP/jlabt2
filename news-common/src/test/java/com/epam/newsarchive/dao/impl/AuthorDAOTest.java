package com.epam.newsarchive.dao.impl;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsarchive.dao.AuthorDAO;
import com.epam.newsarchive.entity.Author;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/conf/test-context.xml")
@DatabaseSetup("classpath:AuthorsTestData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseTearDown(value = "classpath:AuthorsTestData.xml", type = DatabaseOperation.DELETE_ALL)
@ActiveProfiles("test")
public class AuthorDAOTest {
	
	private @Autowired AuthorDAO aDao;
	
	private Author author;
	
	@Before
	public void init(){
		author = new Author(4, "test_4", LocalDateTime.now());
	}

	@Test
	public void testInsert() {
		try{
			assertTrue(aDao.insert(author) > 0);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	@Transactional(rollbackFor = Exception.class)
	public void setExpiredTest(){
		try{
			aDao.insert(author);
			author.setExpired(author.getExpired().plusMonths(7));
			assertTrue(aDao.setExpired(author));
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test(expected = NullPointerException.class)
	@Transactional(rollbackFor = Exception.class)
	public void setExpiredTest_NULL_DATE(){
		try{
			aDao.insert(author);
			author.setExpired(null);
			assertTrue(aDao.setExpired(author));
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void fetchByNewsIdTest(){
		try{
			assertEquals(aDao.fetchByNewsId(2).getName(), "test_author_2");
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void checkTest(){
		try{
			assertTrue(aDao.check(new Author(2, "test_author_2", LocalDateTime.now())) > 0);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}

}
