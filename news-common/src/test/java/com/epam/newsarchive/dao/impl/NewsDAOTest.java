package com.epam.newsarchive.dao.impl;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsarchive.dao.NewsDAO;
import com.epam.newsarchive.entity.News;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/conf/test-context.xml")
@DatabaseSetup("classpath:NewsTestData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseTearDown(value = "classpath:NewsTestData.xml", type = DatabaseOperation.DELETE_ALL)
@ActiveProfiles("test")
public class NewsDAOTest {

	private @Autowired NewsDAO nDao;
	
	private News news;
	private ArrayList<Long> tagIds;
	private ArrayList<Tag> tags;
	
	@Before
	public void init(){
		news = new News(4L, "test_4", "test_4", "test_4", LocalDateTime.now(), LocalDateTime.now());
		
		tagIds = new ArrayList<>();
		tagIds.add(1L);
		tagIds.add(2L);
		
		tags = new ArrayList<>();
		tags.add(new Tag(1, "test_tag_1"));
		tags.add(new Tag(2, "test_tag_2"));
	}
	
	@Test
	public void testFetch() {
		try{
			ArrayList<News> list = nDao.fetch();
			assertTrue(list.size() > 0);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	@Transactional(rollbackFor = Exception.class)
	public void editTest(){
		try{

			ArrayList<News> arrivals = nDao.fetch();
			
			News comparedArr = arrivals.get(0);
			news.setId(comparedArr.getId());
			
			nDao.edit(news);
			news = nDao.fetchById(news.getId());
			
			assertTrue(!nDao.fetchById(news.getId()).equals(comparedArr));
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void insertTest(){
		try{
			assertTrue(nDao.insert(news) > 0);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void linkToTagTest(){
		try{
			assertTrue(nDao.linkToTag(1, tagIds));
			assertTrue(nDao.unlinkFromTag(1));
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void linkToAuthorTest(){
		try{
			assertTrue(nDao.linkToAuthor(2, 2));
			assertTrue(nDao.unlinkFromAuthor(2));
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void unlinkFromTagTest(){
		try{
			assertTrue(nDao.linkToTag(1, tagIds));
			assertTrue(nDao.unlinkFromTag(1));
			
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void unlinkFromAuthorTest(){
		try{
			assertTrue(nDao.linkToAuthor(2, 2));
			assertTrue(nDao.unlinkFromAuthor(2));
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void fetchByIdTest(){
		try{
			News arrival = nDao.fetchById(1);
			assertTrue(arrival != null);
			assertEquals(arrival.getTitle(), "test_title_1");
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void fetchByTagsTest(){
		
		try{
			assertTrue(nDao.linkToTag(1, tagIds));
			
			ArrayList<News> newsControl = nDao.fetchByTags(tags, "test_author_1");
			assertEquals(newsControl.size(), 2);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		} finally{
			try {
				assertTrue(nDao.unlinkFromTag(1));
			} catch (ExceptionDAO e) {
				fail("DAO execution failed");
			}
		}
	}
	
	@Test
	public void countNewsTest(){
		try{
			assertEquals(nDao.countNews(), 3);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}

}
