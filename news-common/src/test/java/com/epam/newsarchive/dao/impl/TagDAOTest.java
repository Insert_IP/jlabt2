package com.epam.newsarchive.dao.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;

import com.epam.newsarchive.dao.TagDAO;
import com.epam.newsarchive.entity.Tag;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/conf/test-context.xml")
@DatabaseSetup("classpath:TagsTestData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseTearDown(value = "classpath:TagsTestData.xml", type = DatabaseOperation.DELETE_ALL)
@ActiveProfiles("test")
public class TagDAOTest {

	private @Autowired TagDAO tDao;
	
	private Tag tag;
	private ArrayList<Tag> tags;
	
	@Before
	public void init(){
		tag = new Tag();
		tag.setTag("Art");
		
		tags = new ArrayList<>();
		tags.add(tag);
	}
	
	@Test
	public void testCheckTag() {
		try{
			assertTrue(tDao.checkTag(tag) > 0);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void insertTag(){
		try{
			assertTrue(tDao.insert(new Tag(4, "Claymore")) > 0);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void insertTagsTest(){
		try{
			ArrayList<Integer> ids = tDao.insertTags(tags);
			assertTrue(ids.size() > 0);
			assertTrue(ids.get(0) != 0);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void fetchByNewsIdTest(){
		try{
			assertTrue(tDao.fetchByNewsId(1).size() > 0);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}

}
