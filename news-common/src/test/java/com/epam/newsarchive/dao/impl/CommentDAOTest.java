package com.epam.newsarchive.dao.impl;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsarchive.dao.CommentDAO;
import com.epam.newsarchive.entity.Comment;
import com.epam.newsarchive.exception.ExceptionDAO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/conf/test-context.xml")
@DatabaseSetup("classpath:CommentsTestData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseTearDown(value = "classpath:CommentsTestData.xml", type = DatabaseOperation.DELETE_ALL)
@ActiveProfiles("test")
public class CommentDAOTest {
	
	private @Autowired CommentDAO cDao;
	
	private Comment comment;
	
	@Before
	public void init(){
		comment = new Comment(4, "test_4", LocalDateTime.now());
	}

	@Test
	public void insertTest() {
		try{
			assertTrue(cDao.insert(comment, 1));
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void deleteTest() {
		try{
			assertTrue(cDao.delete(2));
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}
	
	@Test
	public void fetchTest() {
		try{
			assertTrue(cDao.fetch(1).size() > 0);
		} catch(ExceptionDAO e){
			fail("DAO execution failed");
		}
	}

}
