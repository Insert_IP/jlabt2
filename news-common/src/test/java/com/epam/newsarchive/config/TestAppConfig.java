package com.epam.newsarchive.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;

@Configuration
@ComponentScan("com.epam.newsarchive")
@PropertySource("classpath:/conf/database-test.properties")
@EnableTransactionManagement
@Profile("test")
public class TestAppConfig {
	
	private @Autowired Environment env;

	@Bean(name="dataSource")
	public DataSource dataSource(){
		BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName(env.getProperty("db.driverClassName"));
		ds.setUrl(env.getProperty("db.url"));
		ds.setUsername(env.getProperty("db.user"));
		ds.setPassword(env.getProperty("db.password"));
		ds.setInitialSize(Integer.parseInt(env.getProperty("db.initialSize")));
		return ds;
	}
	
	@Bean(name="transactionManager")
	public DataSourceTransactionManager transactionManager(){
		DataSourceTransactionManager dstm = new DataSourceTransactionManager();
		dstm.setDataSource(dataSource());
		return dstm;
	}
	
	@Bean(name = "dbUnitDatabaseConfig")
	public DatabaseConfigBean dbUnitDatabaseConfig(){
		DatabaseConfigBean dcb = new DatabaseConfigBean();
		dcb.setDatatypeFactory(new Oracle10DataTypeFactory());
		dcb.setCaseSensitiveTableNames(false);
		dcb.setQualifiedTableNames(false);
		return dcb;
	}
	
	@Bean(name = "dbUnitDatabaseConnection")
	public DatabaseDataSourceConnectionFactoryBean dbUnitDatabaseConnection(){
		DatabaseDataSourceConnectionFactoryBean dcf = new DatabaseDataSourceConnectionFactoryBean();
		dcf.setDatabaseConfig(dbUnitDatabaseConfig());
		dcf.setDataSource(dataSource());
		dcf.setSchema("TESTS");
		return dcf;
	}

}
