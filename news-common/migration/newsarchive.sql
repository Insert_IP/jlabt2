--------------------------------------------------------
--  File created - Tuesday-March-15-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table AUTHORS
--------------------------------------------------------

  CREATE TABLE "MAJOODE"."AUTHORS" 
   (	"AUT_AUTHOR_ID" NUMBER, 
	"AUT_AUTHOR_NAME" NVARCHAR2(30), 
	"AUT_EXPIRED" TIMESTAMP (6)
   ) 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "MAJOODE"."COMMENTS" 
   (	"CMT_COMMENT_ID" NUMBER, 
	"CMT_NEWS_ID" NUMBER, 
	"CMT_COMMENT_TEXT" NVARCHAR2(100), 
	"CMT_CREATION_DATE" TIMESTAMP (6)
   )
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

  CREATE TABLE "MAJOODE"."NEWS" 
   (	"NWS_NEWS_ID" NUMBER(*,0), 
	"NWS_TITLE" NVARCHAR2(100), 
	"NWS_SHORT_TEXT" NVARCHAR2(100), 
	"NWS_FULL_TEXT" NVARCHAR2(2000), 
	"NWS_CREATION_DATE" TIMESTAMP (6), 
	"NWS_MODIFICATION_DATE" TIMESTAMP (6)
   )
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table NEWS_AUTHORS
--------------------------------------------------------

  CREATE TABLE "MAJOODE"."NEWS_AUTHORS" 
   (	"NA_NEWS_ID" NUMBER, 
	"NA_AUTHOR_ID" NUMBER
   )
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table NEWS_TAGS
--------------------------------------------------------

  CREATE TABLE "MAJOODE"."NEWS_TAGS" 
   (	"NT_NEWS_ID" NUMBER, 
	"NT_TAG_ID" NUMBER
   )
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------

  CREATE TABLE "MAJOODE"."ROLES" 
   (	"RLS_USER_ID" NUMBER, 
	"RLS_ROLE_NAME" NVARCHAR2(50)
   )
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table TAGS
--------------------------------------------------------

  CREATE TABLE "MAJOODE"."TAGS" 
   (	"TAG_ID" NUMBER, 
	"TAG_NAME" NVARCHAR2(30)
   )
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "MAJOODE"."USERS" 
   (	"USR_USER_ID" NUMBER, 
	"USR_USER_NAME" NVARCHAR2(50), 
	"USR_LOGIN" NVARCHAR2(30), 
	"USR_PASSWORD" NVARCHAR2(30)
   )
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index AUTHOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MAJOODE"."AUTHOR_PK" ON "MAJOODE"."AUTHORS" ("AUT_AUTHOR_ID") 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index COMMENTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MAJOODE"."COMMENTS_PK" ON "MAJOODE"."COMMENTS" ("CMT_COMMENT_ID") 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index NEWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MAJOODE"."NEWS_PK" ON "MAJOODE"."NEWS" ("NWS_NEWS_ID") 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index TAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MAJOODE"."TAG_PK" ON "MAJOODE"."TAGS" ("TAG_ID") 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index USERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MAJOODE"."USERS_PK" ON "MAJOODE"."USERS" ("USR_USER_ID") 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table AUTHORS
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."AUTHORS" ADD CONSTRAINT "AUTHORS_PK" PRIMARY KEY ("AUT_AUTHOR_ID");
  ALTER TABLE "MAJOODE"."AUTHORS" MODIFY ("AUT_AUTHOR_NAME" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."AUTHORS" MODIFY ("AUT_AUTHOR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."COMMENTS" MODIFY ("CMT_NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."COMMENTS" ADD CONSTRAINT "COMMENTS_PK" PRIMARY KEY ("CMT_COMMENT_ID");
  ALTER TABLE "MAJOODE"."COMMENTS" MODIFY ("CMT_CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."COMMENTS" MODIFY ("CMT_COMMENT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."COMMENTS" MODIFY ("CMT_COMMENT_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."NEWS" ADD CONSTRAINT "NEWS_PK" PRIMARY KEY ("NWS_NEWS_ID");
  ALTER TABLE "MAJOODE"."NEWS" MODIFY ("NWS_MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."NEWS" MODIFY ("NWS_CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."NEWS" MODIFY ("NWS_FULL_TEXT" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."NEWS" MODIFY ("NWS_SHORT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."NEWS" MODIFY ("NWS_TITLE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_AUTHORS
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."NEWS_AUTHORS" MODIFY ("NA_AUTHOR_ID" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."NEWS_AUTHORS" MODIFY ("NA_NEWS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_TAGS
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."NEWS_TAGS" MODIFY ("NT_NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."NEWS_TAGS" MODIFY ("NT_TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."ROLES" MODIFY ("RLS_ROLE_NAME" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."ROLES" MODIFY ("RLS_USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TAGS
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."TAGS" ADD CONSTRAINT "TAGS_PK" PRIMARY KEY ("TAG_ID");
  ALTER TABLE "MAJOODE"."TAGS" MODIFY ("TAG_NAME" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."TAGS" MODIFY ("TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."USERS" ADD CONSTRAINT "USERS_PK" PRIMARY KEY ("USR_USER_ID");
  ALTER TABLE "MAJOODE"."USERS" MODIFY ("USR_PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."USERS" MODIFY ("USR_LOGIN" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."USERS" MODIFY ("USR_USER_NAME" NOT NULL ENABLE);
  ALTER TABLE "MAJOODE"."USERS" MODIFY ("USR_USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."COMMENTS" ADD CONSTRAINT "COMMENTS_FK1" FOREIGN KEY ("CMT_NEWS_ID")
	  REFERENCES "MAJOODE"."NEWS" ("NWS_NEWS_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHORS
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."NEWS_AUTHORS" ADD CONSTRAINT "NEWS_AUTHOR_FK1" FOREIGN KEY ("NA_NEWS_ID")
	  REFERENCES "MAJOODE"."NEWS" ("NWS_NEWS_ID") ENABLE;
  ALTER TABLE "MAJOODE"."NEWS_AUTHORS" ADD CONSTRAINT "NEWS_AUTHOR_FK2" FOREIGN KEY ("NA_AUTHOR_ID")
	  REFERENCES "MAJOODE"."AUTHORS" ("AUT_AUTHOR_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAGS
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."NEWS_TAGS" ADD CONSTRAINT "NEWS_TAG_FK1" FOREIGN KEY ("NT_NEWS_ID")
	  REFERENCES "MAJOODE"."NEWS" ("NWS_NEWS_ID") ENABLE;
  ALTER TABLE "MAJOODE"."NEWS_TAGS" ADD CONSTRAINT "NEWS_TAG_FK2" FOREIGN KEY ("NT_TAG_ID")
	  REFERENCES "MAJOODE"."TAGS" ("TAG_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "MAJOODE"."ROLES" ADD CONSTRAINT "TABLE1_FK1" FOREIGN KEY ("RLS_USER_ID")
	  REFERENCES "MAJOODE"."USERS" ("USR_USER_ID") ENABLE;

--------------------------------------------------------
--  Sequence for AUTHORS
--------------------------------------------------------

CREATE SEQUENCE AUT_SEQ;
--------------------------------------------------------
--  Sequence for COMMENTS
--------------------------------------------------------

CREATE SEQUENCE CMT_SEQ;
--------------------------------------------------------
--  Sequence for NEWS
--------------------------------------------------------

CREATE SEQUENCE NWS_SEQ;
--------------------------------------------------------
--  Sequence for ROLES
--------------------------------------------------------

CREATE SEQUENCE RLS_SEQ;
--------------------------------------------------------
--  Sequence for TAGS
--------------------------------------------------------

CREATE SEQUENCE TAG_SEQ;
--------------------------------------------------------
--  Sequence for USERS
--------------------------------------------------------

CREATE SEQUENCE USR_SEQ;