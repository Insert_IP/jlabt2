package com.epam.newsarchive.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.FacadeUserService;
import com.epam.newsarchive.transfer.UserTO;

@Controller
public class UserController {
	
	private @Autowired FacadeUserService service;
	
	@RequestMapping(value = {"/", "/login"})
	public String showLogin(){
		return "login";
	}
	
	@RequestMapping(value = "/viewRegister")
	public ModelAndView showRegistration(){
		return new ModelAndView("register", "user", new UserTO());
	}
	
	@RequestMapping(value = "/register")
	public String registerUser(@ModelAttribute("user")UserTO user){
		try {
			service.registerUser(user);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return "login";
	}
	
	
}
