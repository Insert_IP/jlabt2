package com.epam.newsarchive.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsarchive.exception.ServiceException;
import com.epam.newsarchive.service.FacadeNewsService;
import com.epam.newsarchive.transfer.NewsTO;

@Controller
public class NewsController {
	
	private final Logger LOG = Logger.getLogger(NewsController.class);
	
	private @Autowired FacadeNewsService service;
	
	@Secured(value="ROLE_ADMIN")
	@RequestMapping(value = "/viewArrivals")
	public String viewMainPage(Model model, HttpSession session){
		ArrayList<NewsTO> list = null;
		try {
			list = service.fetchArrivals();
		} catch (ServiceException e) {
			LOG.error("Operation Failed\n" + e);
		}
		model.addAttribute("news", list);
		session.setAttribute("news", list);
		return "personList";
	}
	
	@SuppressWarnings("unchecked")
	@Secured(value="ROLE_ADMIN")
	@RequestMapping(value = "/viewArrival")
	public String getArrival(Model model, @RequestParam("id")long id, HttpSession session){
		ArrayList<NewsTO> list = (ArrayList<NewsTO>)session.getAttribute("news");
		for(NewsTO arrival : list){
			if(arrival.getNews().getId().equals(id)){
				model.addAttribute("arrival", arrival);
				break;
			}
		}
		return "arrival";
	}
}
