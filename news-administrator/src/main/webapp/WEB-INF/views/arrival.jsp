<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table border=1 width="100%">
		<tr>
			<td>${arrival.author.name}</td>
			<td>${arrival.news.creation}</td>
		</tr>
		<tr>
			<td colspan="2">${arrival.news.title}</td>
		</tr>
		<tr>
			<td colspan="2">${arrival.news.sht_text}</td>
		</tr>
		<tr>
			<td colspan="2">${arrival.news.text}</td>
		</tr>
	</table>
</body>
</html>