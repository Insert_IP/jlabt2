<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<f:form action="register" method="POST" modelAttribute="user">
		<table border="1">
			<tr>
				<td><f:label path="user.username">Username</f:label></td>
				<td><f:input path="user.username"/></td>
			</tr>
			<tr>
				<td><f:label path="user.login">Login</f:label></td>
				<td><f:input path="user.login"/></td>
			</tr>
			<tr>
				<td><f:label path="user.password">Password</f:label></td>
				<td><f:input path="user.password" /></td>
			</tr>
			<tr>
				<f:select path="role.roleName">
					<f:option value="ROLE_ADMIN"/>
					<f:option value="ROLE_USER"/>
				</f:select>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</f:form>
</body>
</html>