<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="/news-administrator/resources/style.css">
</head>
<body>
	<c:forEach var="elem" items="${news}">
		<div class="arrival" onclick="location.href='viewArrival?id=${elem.news.id}'">
			<table border=1 width="100%">
				<tr>
					<td>${elem.author.name}</td>
					<td>${elem.news.creation.toLocalDate()}</td>
				</tr>
				<tr>
					<td colspan="2">${elem.news.title}</td>
				</tr>
				<tr>
					<td colspan="2">${elem.news.sht_text}</td>
				</tr>
			</table>
		</div>
	</c:forEach>

	<a href="admin">Move to admin</a>
</body>
</html>