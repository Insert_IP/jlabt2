<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<form name='loginForm' action='<c:url value="/j_spring_security_check"/>' method='POST'>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<table>
			<tr>
				<td><i>Username</i>:</td>
				<td><input type="text" name="j_username" /></td>
			</tr>
			<tr>
				<td><i>Password</i>:</td>
				<td><input type="password" name="j_password" /></td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit" value="Login" /> or <a href="viewRegister">Register</a></td>
			</tr>
		</table>
	</form>
</body>
</html>