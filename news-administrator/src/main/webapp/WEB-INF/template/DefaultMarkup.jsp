<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="/news-administrator/resources/style.css">
</head>
<body>
	<div class="body">
		<div class="header">
			<tiles:insertAttribute name="header" />
		</div>
		
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<div class="menu">
				<tiles:insertAttribute name="menu" />
			</div>
		</sec:authorize>
		
		<div class="main">
			<tiles:insertAttribute name="body" />
		</div>
		
		<div class="footer">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>